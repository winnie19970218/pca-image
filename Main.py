from sklearn.decomposition import PCA
from PIL import Image
import matplotlib.pyplot as plt
from sklearn.preprocessing import StandardScaler

#Read file
fp = open("picture.jpg", "rb")
im = Image.open(fp)
w, h = im.size
print('Image width:{width} pixel, Image height:{height} pixel'.format(width=w, height=h))
#Convert RGB Image to Gray Image
grayim = im.convert('L')
# Original Image
plt.subplot(1, 2, 1);
plt.imshow(im);
plt.xlabel('%s x %s components' %(w,h), fontsize = 14)
plt.title('Original Image', fontsize = 20);
plt.xticks([])
plt.yticks([])
# Gray Image
plt.subplot(1, 2, 2);
plt.imshow(grayim,plt.cm.gray);
plt.xlabel('%s x %s components' %(w,h), fontsize = 14)
plt.title('Gray Image', fontsize = 20);
plt.xticks([])
plt.yticks([])
plt.show()

#Standard data
scaler = StandardScaler()
grayim = scaler.fit_transform(grayim)

#PCA training and inverse the dimensionality reduction data to the original data
pca = PCA(.95)
lower_dimensional_data = pca.fit_transform(grayim)
componentsA= pca.n_components_
print("95% of Explained Variance Components:",pca.n_components_)
approximationA = pca.inverse_transform(lower_dimensional_data)

pca = PCA(.85)
lower_dimensional_data = pca.fit_transform(grayim)
componentsB= pca.n_components_
print("85% of Explained Variance Components:",pca.n_components_)
approximationB = pca.inverse_transform(lower_dimensional_data)

pca = PCA(.75)
lower_dimensional_data = pca.fit_transform(grayim)
componentsC= pca.n_components_
print("75% of Explained Variance Components:",pca.n_components_)
approximationC = pca.inverse_transform(lower_dimensional_data)

# Original Image
plt.subplot(2, 2, 1);
plt.imshow(grayim,plt.cm.gray);
plt.xlabel('%s x %s components' %(w,h), fontsize = 14)
plt.title('Gray Image', fontsize = 20);
plt.xticks([])
plt.yticks([])
# 95% of Explained Variance
plt.subplot(2, 2, 2);
plt.imshow(approximationA,cmap = plt.cm.gray);
plt.xlabel('%s components' %(componentsA), fontsize = 14)
plt.title('95% Variance', fontsize = 20);
plt.xticks([])
plt.yticks([])
# 85% of Explained Variance
plt.subplot(2, 2, 3);
plt.imshow(approximationB,cmap = plt.cm.gray);
plt.xlabel('%s components' %(componentsB), fontsize = 14)
plt.title('85% Variance', fontsize = 20);
plt.xticks([])
plt.yticks([])
# 75% of Explained Variance
plt.subplot(2, 2, 4);
plt.imshow(approximationC,cmap = plt.cm.gray);
plt.xlabel('%s components' %(componentsC), fontsize = 14)
plt.title('75% Variance', fontsize = 20);
plt.xticks([])
plt.yticks([])
plt.tight_layout()
plt.show()